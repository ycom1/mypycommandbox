import sys
from PyQt6.QtWidgets import QApplication
from mvp.model import CommandListModel
from mvp.view import CommandListView
from mvp.presenter import Presenter

if __name__ == '__main__':
    app = QApplication(sys.argv)
    model = CommandListModel()
    view  = CommandListView()
    presenter = Presenter(model, view)
    view.setFocus()
    view.show()
    sys.exit(app.exec())


