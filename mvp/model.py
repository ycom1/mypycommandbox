# this model has influence over a list of commands
# first as variables, then later getting them from and to a file
import os
from os.path import exists
from PyQt6 import QtCore

home_dir = os.path.expanduser("~")
cmd_file = '.command_list_file'
file_exists = exists(home_dir+'/'+cmd_file)
file_path = home_dir+'/'+cmd_file
default_cmd_list = ['echo $PATH', 'uname -a', 'echo $HOME']

if file_exists:
    print("file exists")
    with open(file_path,'r+') as file:
        file_content = file.read().splitlines()
        print("content:"+str(file_content))
        content_len  = len(file_content)
        print(str(content_len)+' entries ')
        if content_len == 0:
            for line in default_cmd_list:
                file.write(line+'\n')
            file.close()
        else:
            with open(file_path,'r+') as file: 
                file_content = file.read().splitlines()
                default_cmd_list = file_content
                file.close()    
else:
    print("file does not exist") 
    with open(file_path,'w+') as file:
        for line in default_cmd_list:
            file.write(line+'\n')
        file.close()
        with open(file_path,'r+') as file:   
            file_content = file.read().splitlines()
            default_cmd_list = file_content  
            file.close()    

class CommandListModel(QtCore.QAbstractListModel):
    def __init__(self, icl = default_cmd_list):
        super(CommandListModel, self).__init__()
        self.init_command_list = icl
        print('init list data:', self.init_command_list)
        
        print('cmd path:', home_dir+'/'+cmd_file)
     
    def set_command_list(self):
        # read file
        # if not emty read out init_command_list
        # else pass init_command_list from class initialisation
        command_list = default_cmd_list
        return command_list  
       
    def add_command(self, new_command):
        print('adding command')
        self.command_list = default_cmd_list
        self.command_list.append(str(new_command))
        print("new list item:", new_command)
        print("new list:", self.command_list)
        print('list to save:', self.command_list)
        with open(file_path,'w+') as file:
            for line in self.command_list:
                file.write(line+'\n')
        file.close()
        print('saved'); 

    def remove_command(self, target_command):
        print('removing command')
        self.command_list = default_cmd_list
        self.command_list.remove(str(target_command))
   
        with open(file_path,'w+') as file:
            for line in self.command_list:
                file.write(line+'\n')
        file.close()
        print('saved');      
