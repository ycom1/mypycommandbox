from PyQt6.QtCore import QObject, pyqtSignal
from PyQt6.QtWidgets import (
    QWidget,
    QLineEdit,
    QPushButton,
    QVBoxLayout,
    QListWidget,
   
)
from PyQt6 import QtGui
from mvp.model import CommandListModel
from mvp.view import CommandListView

class Presenter(QObject):
    commandAdded  = pyqtSignal(str)

    def __init__(self, model, view):
        super().__init__() 
        self.my_model = model
        my_commands = self.my_model.set_command_list()

        self.my_view = view
        self.my_view.show_command_list(my_commands)
        
        self.my_view.add_button.clicked.connect(lambda: self.pass_input_value())
        self.my_view.add_button.clicked.connect(lambda: self.my_view.update_list_view(my_commands))
        
        self.my_view.delete_button.clicked.connect(lambda: self.remove_selected_value())
       
    def pass_input_value(self):
        cmd = self.my_view.get_command_string()
        print('cmd:', cmd)
        self.my_model.add_command(new_command=cmd)

    def remove_selected_value(self):
        print('removing')
        item = self.my_view.list.currentItem()
        print('removing:', item.text())
        self.my_model.remove_command(target_command=item.text()) 
        self.my_view.update_list_view(self.my_model.set_command_list())

  
        
       
 

