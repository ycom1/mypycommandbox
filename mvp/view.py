from PyQt6.QtWidgets import (
    QWidget,
    QLineEdit,
    QPushButton,
    QVBoxLayout,
    QListWidget
)

class CommandListView(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
 
    def initUI(self):
        self.setWindowTitle('shell command list')
        self.setGeometry(100,100, 800, 600)
        self.layout = QVBoxLayout()

        self.list = QListWidget()
        self.layout.addWidget(self.list)         
 
        self.input1 = QLineEdit('')
        self.layout.addWidget(self.input1)
        self.input1.setPlaceholderText("new command") 

        self.add_button = QPushButton('add')
        self.layout.addWidget(self.add_button)

        self.delete_button = QPushButton('delete entry')
        self.layout.addWidget(self.delete_button)

        self.setLayout(self.layout)

    def show_command_list(self, cmdlist):
        for cmd in cmdlist:
            self.list.addItem(cmd)
            print('->', cmd)

    def update_list_view(self, cmdlist):
        self.list.clear()
        for cmd in cmdlist:
            self.list.addItem(cmd)
            print('->', cmd)
        self.input1.clear()    

    def get_command_string(self):
        new_command = self.input1.text()
        return new_command

    